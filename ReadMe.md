What Did I learn?
<br>
Throughout this class, I have learned that in order to get a “game” working, you first probably need to get a decent playable prototype. I believe that I was expecting a AAA game to be ready and finished by the end of the few months that we worked on it, and honestly I now realize how lofty that vision was. (especially sense I had never worked with monogame, XNA or even C#.) I also realized that a prototype should be a stepping stone toward the final game and should be made. I got caught up in the final goal, a AAA title for mobile. This way of thinking discouraged me and made the job harder because when I should have been working on a single bag type, fully fleshing it out, and building a prototype around that one bag then extending it out, I got stuck in how other bags would react to the situation. Instead of worrying about gameplay, win conditions and fun factor, I got stuck on animation. Instead of prototyping a weather system with the basic bag, I made four bags to test and play with, throwing off other milestones and allowing them to pile up. Once they piled up, I was behind. Once I was behind, little things like bugs prevented me from any further progress, thus continuing the loop of feature creep and demotivation.
<br>
The question that comes to mind after how the semester went is this: “How do you eat an elephant?”
<br>
I definitely didn’t eat it one bite at a time and I was stressed, miserable, sloppy and I lost motivation. Because of this, I have also learned that I need help with integrity, time management and accountability. I want to take responsibility for the lack of progress, because in doing so I take responsibility for the honestly shoddy work that has been put into the project this semester. Hopefully you’ll get to play it one day when it is finished!
<br>
Milestones:<br>
	3/23:<br>
•	Bag classes done with different vars<br>
o	Health<br>
o	GravityCoeff<br>
o	Event handlers<br>
•	Basic Platik bag physics<br>
•	Basic follow camera based on 2.5D layout<br>
•	A Runable piece of code<br>
4/6<br>
•	Physics Done<br>
•	Collision<br>
•	Programmer Art (Woo)<br>
•	A playable Level of a certain distance (prolly country side)<br> 
(scrapped, had animation problems)<br>
--------------------------------- How far I got (due to the above situation) ---------------------------------<br>
-------------------------------------------- Also, too much ambition ---------------------------------------------<br>
<br>
4/20<br>
•	Polishing art<br>
•	Polish Day/Night<br>
•	Polish Weather<br>
•	Bugs?<br>
5/4<br>
•	Catch up week<br>
•	Whatever I missed, get this week<br>
•	Flesh out bugs n stuff<br>
•	Hopefully also sound<br>

Thank you, Dr. Landon for introducing me to the art of game engine design! It has intrigued me, and I want to continue working with them because I find it complicatedly fun. I don’t think I would have gotten back into XNA dev if it wasn’t for your class. Also, because of what you taught me (and in spite of the situation this semester) I feel more confident in my ability to program physics into a game.<br>
Thanks for your time and knowledge!<br>
Jacob<br>

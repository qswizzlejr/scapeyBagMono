﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScapeyBag.Bags;
using System;

namespace ScapeyBag
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Model testbox;
        Texture2D testText;
        Bag testBag = new Bag();

        //bools
        bool bEndGame = false;

        //delays
        float jumpTimer = 0f;

        //game vectors
        Matrix world = Matrix.Identity;
        Matrix view = Matrix.Identity;
        Matrix proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(30f), 1.33f, 1f, 10000f);


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            base.Initialize();


        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            testbox = Content.Load<Model>("SM_BagPlane");
            testText = Content.Load<Texture2D>("T_Plastik");

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //Game Code
            if (!bEndGame)
            {
                // TODO: Add your update logic here
                if (Keyboard.GetState().IsKeyDown(Keys.Space) && jumpTimer <= 0)
                {
                    jumpTimer = 160f;
                    testBag.jump();
                }
                else
                {
                    jumpTimer -= gameTime.ElapsedGameTime.Milliseconds;
                }
                testBag.tick(gameTime);
            }
            else
            {
                //reset game
                if (Keyboard.GetState().IsKeyDown(Keys.R))
                {
                    testBag.Pos = new Vector3(this.Window.ClientBounds.X / 2 - 50, this.Window.ClientBounds.Y / 2 - 50, 0);
                    bEndGame = false;
                }
            }
            Console.WriteLine(testBag.Pos);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            world = Matrix.CreateTranslation(testBag.Pos);
            view = Matrix.CreateLookAt(new Vector3(0, 0, 4500f), Vector3.Zero, Vector3.Up);
            Point bagPos = new Vector2(this.Window.ClientBounds.Width / 2 - 50, -testBag.Pos.Y).ToPoint();

            if (bagPos.Y + 50 <= this.Window.ClientBounds.Height && !bEndGame)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(testText, new Rectangle(bagPos, new Point(100)), Color.White);
                spriteBatch.End();
            }
            else
            {
                GraphicsDevice.Clear(Color.Red);
                bEndGame = true;
            }


            base.Draw(gameTime);
        }
    }
}

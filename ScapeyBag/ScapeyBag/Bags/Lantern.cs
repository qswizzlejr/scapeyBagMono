﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System;

namespace ScapeyBag.Bags
{
    public class Lantern : Bag
    {
        //lamp vars
        bool bIsJumping = false;


        public override void init(Vector3 pos, Vector3 vel, float grav_mult = 1, float drag_mult = 1, float jump_mult = 0, float speed_mult = 1, float _health = 100, BagType type = BagType.Lantern)
        {
            base.init(pos, vel, grav_mult, drag_mult, jump_mult, speed_mult, _health, type);
        }

        public override void tick(GameTime gameTime)
        {
            if (GravMult < 1)
            {
                GravMult += .1f;
            }
            Console.WriteLine(GravMult);
            base.tick(gameTime);
        }

        public override void jump()
        {
            GravMult = -1;
        }

        public bool Jumping
        {
            set { bIsJumping = value; }
            get { return bIsJumping; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

public enum BagType { Plastik, Golden, Lantern, Kite, Default};

namespace ScapeyBag.Bags
{
    public class Bag
    {

        //private vars
        //Describe Movement
        Vector3 velocity;
        Vector3 position;
        Vector3 gravity;
        Vector3 jumpVec = Vector3.Up;
        Vector3 drag = Vector3.Zero;

        //Describe Movement multipliers
        float gravityMult;
        float dragMult;
        float jumpDeltaMult;
        float speedMult;

        //Bag Stuff
        float health;
        bool bIsDead;
        BagType bag_type = BagType.Default;
        
        //default constructor
        public Bag()
        {
            init(Vector3.Zero, Vector3.Zero);
        }

       //initialization function that is overridable to each extension of bag
        public virtual void init(Vector3 pos, Vector3 vel, float grav_mult = 1, float drag_mult = 1.5f, float jump_mult = 350, float speed_mult = 1, float _health = 100f, BagType type = BagType.Default)
        {
            position = pos;
            velocity = vel;
            gravityMult = grav_mult;
            gravity = new Vector3(0, -980f/2, 0) * gravityMult;
            dragMult = drag_mult;
            jumpDeltaMult = jump_mult;
            speedMult = speed_mult;
            health = _health;
            bag_type = type;
        }

        public override string ToString()
        {
            switch(this.bag_type)
            {
                case BagType.Lantern:
                    return "Lantern";
                case BagType.Plastik:
                    return "Plastik";
                case BagType.Kite:
                    return "Kite";
                case BagType.Golden:
                    return "Golden Plastik";
                default:
                    return "NULL";
            }
        }

        //no special thing here... if damage, del health, if no health, death. To be extended later
        public virtual void takeDamage(float damage)
        {
            health -= damage;
            bIsDead = (health <= 0);
        }

        //jump event
        public virtual void jump()
        {
            velocity = (jumpVec * jumpDeltaMult);
        }

        //tick
        public virtual void tick(GameTime gameTime)
        {
            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;
            position += velocity * time;
            velocity += ((gravity * gravityMult) - (drag * dragMult)) * time;
        }

        //Get / Set
        public Vector3 Pos
        {
            set { position = value; }
            get { return position; }
        }

        Vector3 Drag
        {
            set { drag = Vector3.Normalize(value) * dragMult; }
            get { return drag; }
        }

        protected float JumpMult
        {
            set { jumpDeltaMult = value; }
            get { return jumpDeltaMult; }
        }

        protected float GravMult
        {
            set { gravityMult = value; }
            get { return gravityMult;}
        }

        bool Alive
        {
            get { return !bIsDead; }
        }
    }
}
